<?php
    define("HOTE",     "localhost");
    define("BASE",     "worldinfo");
    define("LOGIN",    "root");
    define("PASSWORD", "");



    require_once('../model/DaoCompte.php');
    require_once('../model/DtoCompte.php');
    session_start();

    $_SESSION['hote']=HOTE;
    $_SESSION['base']=BASE;
    $_SESSION['login']=LOGIN;
    $_SESSION['password']=PASSWORD;
    
    $page = "connexion";
    $erreur = "";
    
    
    /* on clôture la session si btn déco est cliqué */
    if(isset($_POST['btnDeconnexion'])){
        $_SESSION['DtoCompte'] = "";
        session_unset();
    }
    
    /* renvoi l'utilisateur vers menu principal si non connecté */
    if(isset($_SESSION['DtoCompte'])){
        header('Location: ./MenuprincipalController.php');
    }
    
    
    /* on vérifie que le formulaire est bien complété et on connecte l'utilisateur */
    if(isset($_POST['btnConnexion'])){
        if(isset($_POST['username'])&& $_POST['username']!=""){
            if(isset($_POST['password'])&& $_POST['password']!=""){
                $daoCompte = new DaoCompte(HOTE,BASE,LOGIN,PASSWORD);

                $daoCompte = new DaoCompte($_SESSION['hote'],$_SESSION['base'],$_SESSION['login'],$_SESSION['password']);

                $dtoCompte = $daoCompte->verifieCompte($_POST['username'],$_POST['password']);
                
                
                if(isset($dtoCompte)){
                    $daoCompte->connectUser($dtoCompte);
                    header('Location: ./MenuprincipalController.php');
                }else{
                    $erreur = "Oups ! Problème d'authentification";
                }
            }else{
                $erreur = "Veuillez saisir votre mot de passe ! ";
            }
        }else{
            $erreur = "Veuillez saisir votre identifiant";
        }
    }

    include('../View/layout.html');