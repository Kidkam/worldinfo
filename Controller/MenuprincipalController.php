<?php
    require_once('../model/DaoCompte.php');
    require_once('../model/DtoCompte.php');

    session_start();

    $page = "menuprincipal";
    $erreur = "";

    /* bloque les utilisateurs non connectés à accéder à cette page */
    if(!isset($_SESSION['DtoCompte'])){
        header('Location: ./FrontController.php');
    }
    
    $dtoCompte = $_SESSION['DtoCompte'];
    $admin = $dtoCompte->getAdmin();
   
    include("../View/layout.html");