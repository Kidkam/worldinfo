<?php
    require_once('../model/DaoCompte.php');
    require_once('../model/DtoCompte.php');
    require_once('../model/DaoIntervention.php');
    require_once('../model/DtoIntervention.php');

    session_start();
 
    
    $page = "demandeIntervention";
    $erreur = "";

    /* empeche un utilisateur non connecté de charger cette page */
    if(!isset($_SESSION['DtoCompte'])){
        header('Location: ./FrontController.php');
    }


    if(isset($_POST['date'])) {
        if(isset($_POST['nom'])) {
            if (isset($_POST['bureau'])) {
                if (isset($_POST['groupOfDefaultRadios'])) {
                    if (isset($_POST['num_ordi'])) {
                        if (isset($_POST['remarque'])) {
                            $daoIntervention = new DaoIntervention($_SESSION['hote'], $_SESSION['base'], $_SESSION['login'], $_SESSION['password']);

                            $dtoIntervention = new DtoIntervention (
                                $_POST['bureau'],
                                $_POST['nom'],
                                $_POST['groupOfDefaultRadios'],
                                $_POST['remarque'],
                                $_POST['date']
                            );

                            $daoIntervention->insertIntervetion($dtoIntervention);

                            echo "Demande d'intervention créee | Ticket numéro : ".$dtoIntervention->getNum();
                        }
                    }
                }
            }
        }
    }

    include("../View/layout.html");