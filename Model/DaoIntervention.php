<?php

/**
 * Class DaoIntervention
 */
class DaoIntervention
{
    private $bdd;
    private $hote;
    private $UserName;
    private $Password;

    /**
     * DaoIntervention constructor.
     * @param $hote
     * @param $base
     * @param $UserName
     * @param $Password
     */
    public function __construct($hote, $base, $UserName, $Password){
        try{
            $this->hote=$hote;
            $this->UserName=$UserName;
            $this->Password=$Password;
            $this->bdd = new PDO('mysql:host='.$hote.';dbname='.$base.';charset=utf8', $UserName, $Password);
            $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (Exception $e){
            die('Error :' . $e->getMessage());
        }
    }
    
    /* fct qui permet de créer une nouvelle demande d'intervention */
    public function insertIntervetion($DtoIntervention){
        
        $request = 'INSERT INTO intervention (salle, nom, description, remarque, dateDemande) values(:t_salle, :t_nom, :t_description, :t_remarque,:t_dateDemande);';
        
        $req = $this->bdd->prepare($request);
        $req->execute( array(
            't_salle' => $DtoIntervention->getSalle(),
            't_nom' => $DtoIntervention->getNom(),
            't_description' => $DtoIntervention->getDescription(),
            't_remarque' => $DtoIntervention->getRemarque(),
            't_dateDemande' => $DtoIntervention->getDateDemande(),
        ));
    }
    
    /* Affichage le suivi d'intervention */
    public function afficherIntervention(){
        
        echo'<table>';
            echo'<tr>';
                echo'<th>Numéro d\'intervention</th>';
                echo'<th>Salle</th>';
                echo'<th>Nom</th>';
                echo'<th>Description</th>';
                echo'<th>Remarques</th>';
                echo'<th>Date de la demande</th>';
            echo'</tr>';
   
        $request = 'SELECT * FROM intervention;';
        $result = $this->bdd->query($request);
        while($data = $result->fetch()){

            echo'</tr>';
                echo '<td>'.$data['num'].'</td>';
                echo '<td>'.$data['salle'].'</td>';
                echo '<td>'.$data['nom'].'</td>';
                echo '<td>'.$data['description'].'</td>';
                echo '<td>'.$data['remarque'].'</td>';
                echo '<td>'.$data['dateDemande'].'</td>';
            echo'</tr>';
        }
        $result->closeCursor();
        echo'</table>';
    }
    
}
