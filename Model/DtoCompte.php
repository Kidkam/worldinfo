<?php

/**
 * Class DtoCompte
 */
class DtoCompte{

    private $idCompte;
    private $UserName;
    private $Password;
    private $Admin;

    /**
     * DtoCompte constructor.
     * @param $UserName
     * @param $Password
     * @param $Admin
     */
    public function __construct($UserName, $Password, $Admin){
            $this->UserName = $UserName;
            $hash = password_hash($Password, PASSWORD_DEFAULT);
            $this->Password = $hash;
            $this->Admin = $Admin;
    }

    /**
     * @return mixed
     */
    public function getIdCompte()
    {
        return $this->idCompte;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->UserName;
    }

    /**
     * @param mixed $UserName
     */
    public function setUserName($UserName)
    {
        $this->UserName = $UserName;
    }

    /**
     * @return bool|string
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @param bool|string $Password
     */
    public function setPassword($Password)
    {
        $this->Password = $Password;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->Admin;
    }

    /**
     * @param mixed $Admin
     */
    public function setAdmin($Admin)
    {
        $this->Admin = $Admin;
    }

}