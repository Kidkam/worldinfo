<?php

/**
 * Class DaoCompte
 */
class DaoCompte{
    
    private $bdd;
    private $hote;
    private $UserName;
    private $Password;

    /**
     * DaoCompte constructor.
     * @param $hote
     * @param $base
     * @param $UserName
     * @param $Password
     */
    public function __construct($hote, $base, $UserName, $Password){
        try{
            $this->hote=$hote;
            $this->UserName=$UserName;
            $this->Password=$Password;
            $this->bdd = new PDO('mysql:host='.$hote.';dbname='.$base.';charset=utf8', $UserName, $Password);
        }catch (Exception $e){
            die('Error :' . $e->getMessage());
        }
    }
    
    /* fct qui connecte l'utilisateur à la session */
    /**
     * @param $dtoCompte
     */
    public function connectUser($dtoCompte){
        $_SESSION['DtoCompte'] = $dtoCompte; 
    }

    /* fct qui permet d'ajouter un utilisateur */
    /**
     * @param $dtocompte
     */
    public function newCompte($dtocompte){
        try{
            $request = 'INSERT INTO compte(UserName,Password, Admin) VALUES(:t_username,:t_password, :t_admin);';
            $stmt = $this->bdd->prepare($request);

            if (!$stmt) print_r($this->bdd->errorInfo());

            $result = $stmt->execute( array(
                't_username' => $dtocompte->getUserName(),
                't_password' => $dtocompte->getPassword(),
                't_admin' => $dtocompte->getAdmin()
           ));

            if (!$result) print_r($stmt->errorInfo());

        }catch (Exception $e){
            die('Error :' . $e->getMessage());
        }
    } 

    /* fct de contrôle d'authentification */
    /**
     * @param $UserName
     * @param $Password
     * @return DtoCompte
     */
    public function verifieCompte($UserName, $Password){
        try{
            $request = 'SELECT * FROM compte WHERE UserName=?;';
            $request = $this->bdd->prepare($request);
            $request->execute(array($UserName));

            $data = $request->fetch();

            if(password_verify($Password,$data['Password'])){
                $DtoCompte = new DtoCompte($data['UserName'],$data['Password'],$data['Admin']);
                return $DtoCompte;

            } 
        }catch (Exception $e){
            die('Error :' . $e->getMessage());
        }                
    }

    /* fct qui permet de supprimer un compte utilisateur */
    /**
     * @param $UserName
     * @return bool
     */
    public function supprimerCompte($UserName)
    {
        try {
            $request = 'SELECT * FROM compte WHERE UserName=?;';
            $request = $this->bdd->prepare($request);
            $request->execute(array($UserName));

            $data = $request->fetch();


            if ($data['UserName'] != null) {
                $request = 'DELETE FROM compte WHERE UserName=?;';
                $request = $this->bdd->prepare($request);
                $request->execute(array($UserName));

                return true;
            }
            return false;

        } catch (Exception $e) {
            die('Error :' . $e->getMessage());
        }
    }

}