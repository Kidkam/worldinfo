<?php

/**
 * Class DtoIntervention
 */
class DtoIntervention{

    private $num;
    private $nom;
    private $salle;
    private $description;
    private $remarque;
    private $dateDemande;

    /**
     * DtoIntervention constructor.
     * @param $salle
     * @param $description
     * @param $remarque
     * @param $dateDemande
     */
    public function __construct($salle, $nom, $description, $remarque, $dateDemande){
        $this->salle = $salle;
        $this->nom = $nom;
        $this->description = $description;
        $this->remarque = $remarque;
        $this->dateDemande = $dateDemande;
    }

    /**
     * @return mixed
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @return mixed
     */
    public function getSalle()
    {
        return $this->salle;
    }

    /**
     * @param mixed $salle
     */
    public function setSalle($salle)
    {
        $this->salle = $salle;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * @param mixed $remarque
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;
    }

    /**
     * @return mixed
     */
    public function getDateDemande()
    {
        return $this->dateDemande;
    }

    /**
     * @param mixed $dateDemande
     */
    public function setDateDemande($dateDemande)
    {
        $this->dateDemande = $dateDemande;
    }

}